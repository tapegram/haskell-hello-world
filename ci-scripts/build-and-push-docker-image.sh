#!/bin/sh
docker login -u gitlab-ci-token -p $1 registry.gitlab.com
docker pull registry.gitlab.com/tapegram/haskell-hello-world:latest || true
docker build --cache-from registry.gitlab.com/tapegram/haskell-hello-world:latest -t haskell-hello-world:latest .

docker tag haskell-hello-world:latest registry.gitlab.com/tapegram/haskell-hello-world:$2
docker tag haskell-hello-world:latest registry.gitlab.com/tapegram/haskell-hello-world:latest

docker push registry.gitlab.com/tapegram/haskell-hello-world:$2
docker push registry.gitlab.com/tapegram/haskell-hello-world:latest