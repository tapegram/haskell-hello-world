#!/bin/sh
printf '%s' "$1" > key.json # Google Cloud service account key
gcloud auth activate-service-account --key-file key.json

gcloud config set project haskell-hello-world-320123
gcloud config set compute/zone us-central1
gcloud config set container/use_client_certificate False

gcloud container clusters get-credentials haskell-hello-world

kubectl delete secret registry.gitlab.com --ignore-not-found=true
kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$2 --docker-password=$3

kubectl apply --prune -f k8s/ -l app=haskell-hello-world --namespace=$4
