# Overview
Setting up a minimal ci/cd pipeline with
1) Gitlab
2) Stack
3) GKE

with a lot of help from docker.


## References
[Creating a staging env in gitlab](https://docs.gitlab.com/12.10/ee/ci/environments.html)
## .gitlab-ci.yml
This is where all of the gitlab ci configuration lives. Unfortunately, it can not be split into smaller components, so it will host all of the job and pipeline definitions.

In order to make it a little slimmer, all of the logic for the jobs will be delegated to scripts that live in [`/ci-scripts`](ci-scripts/)

## Merge Request CI
Jobs run against an open merge request.

### Build
In `.gitlab-ci.yml` the first stage is `mr-build`, where we run `stack build` on a haskell docker image. This is only run on merge requests (see the `only` block)

Because `stack build` can take 30 minutes, we are using the gitlab cache to cache as many stack build artifacts as possible. Note that we also have to set `STACK_ROOT` in the variables otherwise the artifacts are put outside of the project directory and aren't found for the cache.

### TODO
We eventually want to run tests, validate LB migrations and connections to 3rd parties, etc.

### Main CI
Jobs run post merge.

### Package
In this step we build the newest docker image with our app installed. The big problem again is that we have to run `stack build` which takes far too long, but the gitlab cache doesn't work on the docker image (runs out of space immediately).
```dockerfile
FROM haskell:8.10
# Hopefully these steps can be cached (except when the dependencies change)
COPY stack.yaml stack.yaml.lock haskell-hello-world.cabal ./
RUN stack build --only-dependencies

# Actually build the app
COPY . .
RUN stack build
ENTRYPOINT ["stack", "exec", "haskell-hello-world"]
```
So in order to cut down on the build time we split up the docker image layers. We first copy over only the dependency management files and run build only on those dependencies. In gitlab-ci, we use the previously built image as the cache for the new one:
```yml
- docker pull registry.gitlab.com/tapegram/haskell-hello-world:latest || true
- docker build --cache-from registry.gitlab.com/tapegram/haskell-hello-world:latest -t haskell-hello-world:latest .
```

This means that if the previously built image has the same dependencies as the current one, docker will used the cached layers! (Cutting it down from 30ish minutes to 4ish minutes!)

```bash
$ docker build --cache-from registry.gitlab.com/tapegram/haskell-hello-world:latest -t haskell-hello-world:latest .
Step 1/6 : FROM haskell:8.10
8.10: Pulling from library/haskell
0bc3020d05f1: Already exists
98dc064ef29b: Already exists
5461a39004ed: Already exists
7b95779fc113: Already exists
Digest: sha256:5eabc853272db92dcd3b0f0a9eb93436e77d9af7ef59d4d57b342d8d385c8f26
Status: Downloaded newer image for haskell:8.10
 ---> bd33f5ff9bbd
Step 2/6 : COPY stack.yaml stack.yaml.lock haskell-hello-world.cabal ./
 ---> Using cache
 ---> 1a3fa8d4363b
Step 3/6 : RUN stack build --only-dependencies
 ---> Using cache
 ---> 6876fdc0646c
Step 4/6 : COPY . .
 ---> 3b12b1d0d24c
Step 5/6 : RUN stack build
 ---> Running in c0b424e365b0
Building all executables for `haskell-hello-world' once. After a successful build of all of them, only specified executables will be rebuilt.
haskell-hello-world> configure (exe)
Configuring haskell-hello-world-0.1.0.0...
haskell-hello-world> build (exe)
Preprocessing executable 'haskell-hello-world' for haskell-hello-world-0.1.0.0..
Building executable 'haskell-hello-world' for haskell-hello-world-0.1.0.0..
[1 of 1] Compiling Main
Linking .stack-work/dist/x86_64-linux/Cabal-3.2.1.0/build/haskell-hello-world/haskell-hello-world ...
haskell-hello-world> copy/register
Installing executable haskell-hello-world in /.stack-work/install/x86_64-linux/2ca24c1c26dba6540cf937c70ac3055d169d6922cd90b0f478229c732850b95a/8.10.4/bin
Removing intermediate container c0b424e365b0
 ---> d44141328c9d
Step 6/6 : ENTRYPOINT ["stack", "exec", "haskell-hello-world"]
 ---> Running in b24d21c4c10c
 ```

 Afterwards, we just do some image tagging and push the image to the gitlab container registry.

 ### Deploy
 First, I manually created an Autopilot GKE cluster (which was really easy).

 Instead of using the built in GKE integration (because I ran into some issues and didn't want to deal with them), I generated a Google Cloud service account key and added it to the gitlab variables.

Then we auth and connect to the cluster.

Now we do the actual bits!

1) The cluster needs permissions to pull from gitlab's container registry (for the image), so we delete it if it exists, and create it again. Note: we ran into this issue https://gitlab.com/gitlab-org/gitlab/-/issues/20467 and were able to fix it by following the suggestion in the comments to create a gitlab-deploy-token and use the `CI_DEPLOY_USER` and `CI_DEPLOY_PASSWORD` vars instead of the registry ones.
2) Then we apply our k8s manifests!

Our manifests are just a deployment and a service load balancer.

The most interesting part of the k8s manifest is that we point to the `:latest` tag on our images, configure the container to Always pull the image, and also attach the secret we created in the previous step.

```yml
containers:
- name: haskell-hello-world
  image: registry.gitlab.com/tapegram/haskell-hello-world:latest
  imagePullPolicy: Always
  ports:
  - containerPort: 8080
  resources:
    requests:
      cpu: 100m
      memory: 100Mi
    limits:
      cpu: 200m
      memory: 128Mi
imagePullSecrets:
  - name: registry.gitlab.com
```

We have separate deploy steps to stage and prod which are the same k8s except for different namespaces. There is also a stage-test step that will need to be filled with with integration/e2e tests before continuing on to prod.