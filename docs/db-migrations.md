# DB Migrations

## References
 https://stackoverflow.com/questions/45884185/cockroachdb-docker-compose-script-with-sql-commands
 https://gist.github.com/dbist/ebb1f39f580ad9d07c04c3a3377e2bff
 https://wiki.onap.org/display/DW/Liquibase+Docker+Compose+Testing
 https://www.cockroachlabs.com/docs/stable/liquibase.html
 https://github.com/cockroachlabs-field/cockroachdb-remote-client
 https://www.liquibase.org/blog/managing-cockroach-database-schema-changes
 https://github.com/docker/compose/issues/5523

## Local Dev
The top level docker compose will 
1) Spin up a cockroach db insecure single node
2) Use a cockroach db config container to do some basic setup (like creating a database and default user)
3) Use another container to run liquibase migrations against that db

In `/db` we have some example changelogs, a dockerfile for the liquibase image that is aware of the changelogs, and a short script that the image uses to connect to the accompanying cockroach db instance.

The changelogs will need to be the same from local to production. We will need to figure out the best way to organize everything else to handle the differences between local dev with docker compose, and prod environments where we connect to a remote managed db instance.

### CLI
In our Makefile we have some simple commands to manage the dev env
1) `make dev-up` will run docker compose and spin up the db instance and run migrations against it.
2) `make dev-down` takes it down
3) `make dev-restart` is a wrapper around `dev-down` and then `dev-up`
4) `make connect-db` will connect to the running cockroach db container and run the sql client.
## CI
tbd

## Stage 
tbd

## Production
tbd

