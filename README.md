# haskell-hello-world

This project is a template for CI/CD using a handful of specific technologies
1) Haskell
2) GKE
3) Gitlab
4) Docker
5) Liquibase
6) Postgres (read: CockroachDB)

The goal of this repo is to get a lot of hands on experience with building a full CI/CD pipeline and as a result, to get a template I can use for future Haskell projects (though individual details like Haskell might be easy enough to replace).

See [/docks](docs/) for documentation generated while putting everything together.