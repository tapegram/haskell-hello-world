{-# LANGUAGE OverloadedStrings #-}
module WebServer (webServer) where

import           Data.Monoid (mconcat)
import           Web.Scotty  (get, html, param, scotty)

webServer :: IO ()
webServer = scotty 8080 $ get "/api/:word" $ do
    beam <- param "word"
    html $ mconcat ["<h1>Scotty, ", beam, " me up!</h1>"]
