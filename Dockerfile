FROM haskell:8.10
# Hopefully these steps can be cached (except when the dependencies change)
COPY stack.yaml stack.yaml.lock haskell-hello-world.cabal ./
RUN stack build --only-dependencies

# Actually build the app
COPY . .
RUN stack build
ENTRYPOINT ["stack", "exec", "haskell-hello-world"]
