# --------------------------------------
# Local dev with dependencies

.PHONY: dev-up
dev-up:
	docker compose -d --remove-orphans

.PHONY: dev-down
dev-down:
	docker compose down

.PHONY: dev-restart
dev-restart:
	make dev-down
	make dev-up

.PHONY: connect-db
connect-db:
	docker exec -ti crdb ./cockroach sql --insecure -d dev

# --------------------------------------
# Build and run the app locally

.PHONY: run
run:
	stack exec haskell-hello-world